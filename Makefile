proj_dir := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
MOMW_GIT_REV ?= master
MOMW_USER ?= bmomw
MOMW_VER ?= beta
VIRTUAL_ENV ?= $(proj_dir)/venv
MOMW_REV := $(shell cd .. && git describe --tags)

test:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-lint

venv:
	python3 -m venv $(VIRTUAL_ENV)

pip-tools: venv
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location --upgrade pip-tools

pip-hashes: pip-tools
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m piptools compile --strip-extras --generate-hashes req.in --output-file req.txt

pip-requirements: venv
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location -r req.txt

local:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER) --extra-vars momw_db_reset=true --extra-vars local_code_copy=true --extra-vars commit_author="$(COMMIT_AUTHOR)" --extra-vars commit_sha=$(COMMIT_SHA) --vault-password-file ../auth

local-quiet:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER) --extra-vars momw_db_reset=true --extra-vars local_code_copy=true --extra-vars commit_author="$(COMMIT_AUTHOR)" --extra-vars commit_sha=$(COMMIT_SHA) --tags django_app

local-nover:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_db_reset=true --extra-vars local_code_copy=true --extra-vars commit_author="$(COMMIT_AUTHOR)" --extra-vars commit_sha=$(COMMIT_SHA) --vault-password-file ../auth

local-noreset:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER) --extra-vars momw_db_reset=false --extra-vars local_code_copy=true --extra-vars momw_deploy_kind=noreset --extra-vars commit_author="$(COMMIT_AUTHOR)" --extra-vars commit_sha=$(COMMIT_SHA) --extra-vars django_app_ver=$(MOMW_REV) --vault-password-file ../auth

local-noreset-nover:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars django_app_ver=$(MOMW_REV) --extra-vars local_code_copy=true --extra-vars momw_db_reset=false --extra-vars momw_deploy_kind=noreset --extra-vars commit_author="$(COMMIT_AUTHOR)" --extra-vars commit_sha=$(COMMIT_SHA) --vault-password-file ../auth

git:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER) --extra-vars momw_db_reset=true --extra-vars momw_rev=$(MOMW_GIT_REV) --vault-password-file ../auth

git-nover:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER) --extra-vars momw_db_reset=true --vault-password-file ../auth

git-noreset:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER) --extra-vars momw_rev=$(MOMW_GIT_REV) --vault-password-file ../auth

pip-deps-and-uwsgi-ini:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" ansible-playbook momw-$(MOMW_VER).yml --extra-vars username=$(MOMW_USER) --extra-vars momw_ver=$(MOMW_VER)

testing-local: MOMW_VER=testing
testing-local: MOMW_USER=tmomw
testing-local: local
testing-local-noreset: MOMW_VER=testing
testing-local-noreset: MOMW_USER=tmomw
testing-local-noreset: local-noreset
testing-git: MOMW_VER=testing
testing-git: MOMW_USER=tmomw
testing-git: git

beta-local: local
beta-local-quiet: local-quiet
beta-local-noreset: local-noreset
beta-git: git

staging-local: MOMW_USER=stmomw
staging-local: MOMW_VER=staging
staging-local: local-nover
staging-local-noreset: MOMW_USER=stmomw
staging-local-noreset: MOMW_VER=staging
staging-local-noreset: local-noreset-nover
staging-git: MOMW_USER=stmomw
staging-git: MOMW_VER=staging
staging-git: git-nover

prod-git: MOMW_USER=momw
prod-git: MOMW_VER=prod
prod-git: git
